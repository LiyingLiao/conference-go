import json
import pika
from pika.exceptions import AMQPConnectionError
import django
import os
import sys
import time
from django.core.mail import send_mail


sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "presentation_mailer.settings")
django.setup()


def process_approval(ch, method, properties, body):
    loaded_body = json.loads(body)
    message = f'{loaded_body["presenter_name"]}, we"re happy to tell you that your presentation {loaded_body["title"]} has been accepted'
    subject = "Your presentation has been accepted"
    to_email = [loaded_body["presenter_email"]]
    from_email = "admin@conference.go"
    send_mail(subject, message, from_email, to_email, fail_silently=False)


def process_reject(ch, method, properties, body):
    loaded_body = json.loads(body)
    message = f'{loaded_body["presenter_name"]}, we\'re sorry to tell you that your presentation {loaded_body["title"]} has been rejected'
    subject = "Your presentation has been rejected"
    to_email = [loaded_body["presenter_email"]]
    from_email = "admin@conference.go"
    send_mail(subject, message, from_email, to_email, fail_silently=False)


while True:
    try:
        # ALL OF YOUR CODE THAT HANDLES READING FROM THE
        # QUEUES AND SENDING EMAILS
        parameters = pika.ConnectionParameters(host="rabbitmq")
        connection = pika.BlockingConnection(parameters)

        channel = connection.channel()

        # Listen to the approvals queue.
        channel.queue_declare(queue="presentation_approvals")
        channel.basic_consume(
            queue="presentation_approvals",
            on_message_callback=process_approval,
            auto_ack=True,
        )

        # Listen to the rejections queue.
        channel.queue_declare(queue="presentation_rejections")
        channel.basic_consume(
            queue="presentation_rejections",
            on_message_callback=process_reject,
            auto_ack=True,
        )

        # Start consuming the queue msgs.
        channel.start_consuming()
    except AMQPConnectionError:
        print("Could not connect to RabbitMQ")
        time.sleep(2.0)
