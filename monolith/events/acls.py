import json
import re
from xxlimited import Xxo
import requests
from events.keys import OPEN_WEATHER_API_KEY, PEXELS_API_KEY

# weather_url = "https://api.openweathermap.org/data/2.5/weather?lat={lat}&lon={lon}&appid={API key}"

# def get_weather(parameters):
#     weather_url = "https://api.openweathermap.org/data/2.5/weather?lat={lat}&lon={lon}&appid={OPEN_WEATHER_API_KEY}"
#     weather_headers = {"Authorization": OPEN_WEATHER_API_KEY}
#     weather_url = "https://api.openweathermap.org/data/2.5/weather"
#     parameters["appid"] = OPEN_WEATHER_API_KEY
#     r = requests.get(weather_url, headers=weather_headers, params=parameters)
#     content = json.loads(r.content)


def get_weather(city, state):
    # first get the lat and lon
    params = {
        "q": f"{city}, {state}, US",
        "limit": 1,
        "appid": OPEN_WEATHER_API_KEY,
    }
    url = "http://api.openweathermap.org/geo/1.0/direct"
    r = requests.get(url, params=params)
    content = json.loads(r.content)
    try:
        latitude = content[0]["lat"]
        longitude = content[0]["lon"]
    except (KeyError, IndexError):
        return None

    # get the weather
    params = {
        "lat": latitude,
        "lon": longitude,
        "appid": OPEN_WEATHER_API_KEY,
        "units": "imperial",
    }

    url = "https://api.openweathermap.org/data/2.5/weather"
    r = requests.get(url, params=params)
    content = json.loads(r.content)
    try:
        return {
            "description": content["weather"][0]["description"],
            "temp": content["main"]["temp"],
        }
    except (KeyError, IndexError):
        return None


def get_photo(city, state):
    headers = {"Authorization": PEXELS_API_KEY}
    params = {
        "per_page": 1,
        "query": city + " " + state,
    }
    url = "https://api.pexels.com/v1/search"
    response = requests.get(url, params=params, headers=headers)
    content = json.loads(response.content)
    try:
        return {"picture_url": content["photos"][0]["src"]["original"]}
    except (KeyError, IndexError):
        return {"picture_url": None}
